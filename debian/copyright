Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Agustin Martin Domingo
Upstream-Contact: agustin6martin@gmail.com
Source: https://gitlab.com/agmartin/linuxdoc-tools

Files: *
Copyright: 1994 James Clark <jjc@jclark.com>
 1994-1996, Matt Welsh <mdw@cs.cornell.edu>
 1996-1998, Cees de Groot <cg@pobox.com>
 1999-2002, Taketoshi Sano <sano@debian.org>
 2000, Juan Jose Amor
 2007-2024, Agustin Martin Domingo <agmartin@debian.org>
License: PD-SGMLUG

Files: debian/*
Copyright: 1995-1998 Sven Rudolph
 1999-2002, Taketoshi Sano
 2006-2024, Agustin Martin
License: GPL-3+

Files: entity-map/* iso-entities/*
Copyright: 1996-1997, Ken MacLeod
License: Expat-KML

Files: entity-map/sdata/ISOdia
 entity-map/sdata/ISOlat1
 entity-map/sdata/ISOlat2
 entity-map/sdata/ISOtech
 entity-map/sdata/ISOnum
 entity-map/sdata/ISOpub
 iso-entities/entities/ISOamsa
 iso-entities/entities/ISOamsb
 iso-entities/entities/ISOamsc
 iso-entities/entities/ISOamsn
 iso-entities/entities/ISOamso
 iso-entities/entities/ISOamsr
 iso-entities/entities/ISObox
 iso-entities/entities/ISOcyr1
 iso-entities/entities/ISOcyr2
 iso-entities/entities/ISOdia
 iso-entities/entities/ISOgrk1
 iso-entities/entities/ISOgrk2
 iso-entities/entities/ISOgrk3
 iso-entities/entities/ISOgrk4
 iso-entities/entities/ISOlat1
 iso-entities/entities/ISOlat2
 iso-entities/entities/ISOtech
 iso-entities/entities/ISOnum
 iso-entities/entities/ISOpub
Copyright: International Organization for Standardization 1986
License: ISO
 Permission to copy in any form is granted for use with
 conforming SGML systems and applications as defined in
 ISO 8879, provided this notice is included in all copies.

Files: extras/contrib/bk/*
Copyright: 1994-1996, Matt Welsh <mdw@cs.cornell.edu>
License: PD-SGMLUG

Files: extras/contrib/sgml-mode.el
Copyright: 1992, Free Software Foundation, Inc.
License: GPL-3+

Files: extras/contrib/linuxdoc-sgml.el
Copyright: 1996, Free Software Foundation, Inc.
License: GPL-3+

Files: extras/contrib/sgml2index
Copyright: 1996, Fabrizio Polacco <fpolacco@megabaud.fi>
License: GPL-3+

Files: lib/fmt/fmt_html.pl
 perl5lib/LinuxDocTools/Data/Strings_SBCS.pm
 perl5lib/LinuxDocTools/Data/Strings_UTF8.pm
 perl5lib/LinuxDocTools/Lang.pm
 perl5lib/LinuxDocTools/Utils.pm
 perl5lib/LinuxDocTools/Vars.pm
Copyright: 1996-1997, Cees de Groot
 2008-2024, Agustin Martin
License: PD-SGMLUG

Files: lib/fmt/fmt_info.pl
 lib/fmt/fmt_txt.pl
 lib/mappings/info/mapping
Copyright: 1994-1996, Matt Welsh
 1996, Cees de Groot
 1999-2001, Taketoshi Sano
 2008-2020, Agustin Martin
License: PD-SGMLUG

Files: lib/fmt/fmt_latex2e.pl
Copyright: 1994-1996, Matt Welsh
 1996, Cees de Groot
 1999, Kazuyuki Okamoto (euc-jp support in sgml2txt, sgml2html, and sgml2latex)
 1999, Tetsu ONO (euc-jp support in sgml2txt, sgml2html, and sgml2latex)
 1999-2002, Taketoshi Sano
 2000, Juan Jose Amor (Support for PDF files)
 2006-2020, Agustin Martin
License: PD-SGMLUG

Files: lib/fmt/fmt_rtf.pl
Copyright: 1994-1996, Matt Welsh
 1996, Cees de Groot
 1998, Sven Rudolph
 1999-2001, Taketoshi Sano
 2008-2020, Agustin Martin
License: PD-SGMLUG

Files: lib/mappings/latex2e/mapping
Copyright: 1994-1996, Matt Welsh
 1996-1998, Cees de Groot
 1999-2002, Taketoshi Sano
 2000, Juan Jose Amor (Support for PDF files)
 2006-2020, Agustin Martin
License: PD-SGMLUG

Files: lib/mappings/lyx/mapping
Copyright: 1995, Frank Pavageau <frank@via.ecp.fr>
 1997, Jose' Matos <jamatos@novalis.fc.up.pt>
 2008, Agustin Martin <agmartin@debian.org>
License: PD-SGMLUG

Files: perl5lib/LinuxDocTools/Data/Latin1ToSgml.pm
Copyright: 1995, Cees de Groot
 1995, Farzad Farid
 1995, Greg Hankins
 2020, Agustin Martin <agmartin@debian.org>
License: PD-SGMLUG

Files: perl5lib/LinuxDocTools/InfoUtils.pm
Copyright: 2009-2020, Agustin Martin Domingo <agmartin@debian.org>
License: GPL-3+

Files: sgmlpre/sgmlpre.l
Copyright: 1997 Eric S. Raymond <esr@thyrsus.com>
License: PD-SGMLUG

Files: sgmls-1.1/*
Copyright: 1994, James Clark <jjc@jclark.com>
License: SGMLUG-PM

Files: sgmls-1.1/ebcdic.c
Copyright: 1983-1988, C. M. Sperberg-McQueen <u35395@uicvm.uic.edu>
License: SGMLUG-PM

Files: sgmls-1.1/sgmlxtrn.c
Copyright: 1983-1988, Charles F. Goldfarb (assigned to IBM Corporation)
 1988-1991, IBM Corporation
License: SGMLUG-PM

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 In Debian systems you can find a copy under /usr/share/common-licenses.

License: SGMLUG-PM
      Standard Generalized Markup Language Users' Group (SGMLUG)
                         SGML Parser Materials
 .
                              1. License
 .
 SGMLUG hereby grants to any user: (1) an irrevocable royalty-free,
 worldwide, non-exclusive license to use, execute, reproduce, display,
 perform and distribute copies of, and to prepare derivative works
 based upon these materials; and (2) the right to authorize others to
 do any of the foregoing.
 .
                     2. Disclaimer of Warranties
 .
 (a) The SGML Parser Materials are provided "as is" to any USER.  USER
 assumes responsibility for determining the suitability of the SGML
 Parser Materials for its use and for results obtained.  SGMLUG makes
 no warranty that any errors have been eliminated from the SGML Parser
 Materials or that they can be eliminated by USER.  SGMLUG shall not
 provide any support maintenance or other aid to USER or its licensees
 with respect to SGML Parser Materials.  SGMLUG shall not be
 responsible for losses of any kind resulting from use of the SGML
 Parser Materials including (without limitation) any liability for
 business expense, machine downtime, or damages caused to USER or third
 parties by any deficiency, defect, error, or malfunction.
 .
 (b) SGMLUG DISCLAIMS ALL WARRANTIES, EXPRESSED OR IMPLIED, ARISING OUT
 OF OR RELATING TO THE SGML PARSER MATERIALS OR ANY USE THEREOF,
 INCLUDING (WITHOUT LIMITATION) ANY WARRANTY WHATSOEVER AS TO THE
 FITNESS FOR A PARTICULAR USE OR THE MERCHANTABILITY OF THE SGML PARSER
 MATERIALS.
 .
 (c) In no event shall SGMLUG be liable to USER or third parties
 licensed by USER for any indirect, special, incidental, or
 consequential damages (including lost profits).
 .
 (d) SGMLUG has no knowledge of any conditions that would impair its right
 to license the SGML Parser Materials.  Notwithstanding the foregoing,
 SGMLUG does not make any warranties or representations that the
 SGML Parser Materials are free of claims by third parties of patent,
 copyright infringement or the like, nor does SGMLUG assume any
 liability in respect of any such infringement of rights of third
 parties due to USER's operation under this license.

License: Expat-KML
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL KEN MACLEOD BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of Ken MacLeod shall not
 be used in advertising or otherwise to promote the sale, use or other
 dealings in this Software without prior written authorization from
 Ken MacLeod.

License: PD-SGMLUG
 SGMLUG hereby grants to any user: (1) an irrevocable royalty-free,
 worldwide, non-exclusive license to use, execute, reproduce, display,
 perform and distribute copies of, and to prepare derivative works
 based upon these materials; and (2) the right to authorize others to
 do any of the foregoing.
 linuxdoc-tools is derived from linuxdoc-SGML.
 .
 Original Linuxdoc-SGML itself does not have any limitations.
 Everything not having explicit additional conditions can be freely
 used, modified, and redistributed, under the usual disclaimer and
 fair use clauses:
 .
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 * That is, no warranty at all, no liability at all. Use at your own risk.
 * Preserve credits and Copyright notices of the different elements if
   present. Be fair to previous authors.
