/* Declare getopt() and associated variables. */

#ifdef HAVE_GETOPT
#  ifdef __STDC__
     extern int getopt(int argc, char *const *argv, const char *opts);
#  else
     /* Don't use prototypes here in case some system header file has
	a conflicting definition.  Other systems differ on how they
	declare the second parameter. Breaks `strict-prototypes'. */
     extern int getopt();
#  endif /*  __STDC__ */
#else
  /* Using internal getopt */
  extern int getopt(int argc, char **argv, char *opts);
#endif /* HAVE_GETOPT */

extern char *optarg;
extern int optind;
extern int opterr;
